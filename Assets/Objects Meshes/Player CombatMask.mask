%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Player CombatMask
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Cube.009
    m_Weight: 0
  - m_Path: P1_Belt
    m_Weight: 0
  - m_Path: P1_ChestArmor
    m_Weight: 0
  - m_Path: P1_LeftArm
    m_Weight: 0
  - m_Path: P1_Legs
    m_Weight: 0
  - m_Path: P1_RightArm
    m_Weight: 0
  - m_Path: Player
    m_Weight: 0
  - m_Path: Player/HeadLookAt
    m_Weight: 0
  - m_Path: Player/HeadLookAt/HeadLookAt_end
    m_Weight: 0
  - m_Path: Player/IK_LeftHand
    m_Weight: 0
  - m_Path: Player/IK_LeftHand/IK_LeftHand_end
    m_Weight: 0
  - m_Path: Player/IK_RightHand
    m_Weight: 0
  - m_Path: Player/IK_RightHand/IK_RightHand_end
    m_Weight: 0
  - m_Path: Player/Main
    m_Weight: 0
  - m_Path: Player/Main/Hips
    m_Weight: 0
  - m_Path: Player/Main/Hips/LeftUpLeg
    m_Weight: 0
  - m_Path: Player/Main/Hips/LeftUpLeg/LeftLeg
    m_Weight: 0
  - m_Path: Player/Main/Hips/LeftUpLeg/LeftLeg/LeftFeet
    m_Weight: 0
  - m_Path: Player/Main/Hips/LeftUpLeg/LeftLeg/LeftFeet/LeftFeetFingers
    m_Weight: 0
  - m_Path: Player/Main/Hips/LeftUpLeg/LeftLeg/LeftFeet/LeftFeetFingers/LeftFeetFingers_end
    m_Weight: 0
  - m_Path: Player/Main/Hips/RightUpLeg
    m_Weight: 0
  - m_Path: Player/Main/Hips/RightUpLeg/RightLeg
    m_Weight: 0
  - m_Path: Player/Main/Hips/RightUpLeg/RightLeg/RightFeet
    m_Weight: 0
  - m_Path: Player/Main/Hips/RightUpLeg/RightLeg/RightFeet/RightFeetFingers
    m_Weight: 0
  - m_Path: Player/Main/Hips/RightUpLeg/RightLeg/RightFeet/RightFeetFingers/RightFeetFingers_end
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest/LeftArm
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/LeftArm/LeftForearm
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/LeftArm/LeftForearm/LeftHand
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/LeftArm/LeftForearm/LeftHand/LeftHand_end
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/Neck
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest/Neck/Head
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest/Neck/Head/LeftEar
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest/Neck/Head/LeftEar/LeftEar_end
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest/Neck/Head/RightEar
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest/Neck/Head/RightEar/RightEar_end
    m_Weight: 0
  - m_Path: Player/Main/Hips/Spine/Chest/RightArm
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/RightArm/RightForearm
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/RightArm/RightForearm/RightHand
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/RightArm/RightForearm/RightHand/RightHandGrab
    m_Weight: 1
  - m_Path: Player/Main/Hips/Spine/Chest/RightArm/RightForearm/RightHand/RightHandGrab/RightHandGrab_end
    m_Weight: 1
  - m_Path: Player/Main/IK_LeftElbow
    m_Weight: 0
  - m_Path: Player/Main/IK_LeftElbow/IK_LeftElbow_end
    m_Weight: 0
  - m_Path: Player/Main/IK_LeftKnee
    m_Weight: 0
  - m_Path: Player/Main/IK_LeftKnee/IK_LeftKnee_end
    m_Weight: 0
  - m_Path: Player/Main/IK_RightElbow
    m_Weight: 0
  - m_Path: Player/Main/IK_RightElbow/IK_RightElbow_end
    m_Weight: 0
  - m_Path: Player/Main/IK_RightKnee
    m_Weight: 0
  - m_Path: Player/Main/IK_RightKnee/IK_RightKnee_end
    m_Weight: 0
  - m_Path: Player/Main/LeftFeetCTRL
    m_Weight: 0
  - m_Path: Player/Main/LeftFeetCTRL/LeftFeetRollCTRL
    m_Weight: 0
  - m_Path: Player/Main/LeftFeetCTRL/LeftFeetRollCTRL/LeftFeetRollCTRL_end
    m_Weight: 0
  - m_Path: Player/Main/LeftFeetCTRL/LeftHillRoll
    m_Weight: 0
  - m_Path: Player/Main/LeftFeetCTRL/LeftHillRoll/LeftToeRoll
    m_Weight: 0
  - m_Path: Player/Main/LeftFeetCTRL/LeftHillRoll/LeftToeRoll/IK_LeftFeet
    m_Weight: 0
  - m_Path: Player/Main/LeftFeetCTRL/LeftHillRoll/LeftToeRoll/IK_LeftFeet/IK_LeftFeet_end
    m_Weight: 0
  - m_Path: Player/Main/RightFeetCTRL
    m_Weight: 0
  - m_Path: Player/Main/RightFeetCTRL/RightFeetRollCRTL
    m_Weight: 0
  - m_Path: Player/Main/RightFeetCTRL/RightFeetRollCRTL/RightFeetRollCRTL_end
    m_Weight: 0
  - m_Path: Player/Main/RightFeetCTRL/RightHillRoll
    m_Weight: 0
  - m_Path: Player/Main/RightFeetCTRL/RightHillRoll/RIgthToeRoll
    m_Weight: 0
  - m_Path: Player/Main/RightFeetCTRL/RightHillRoll/RIgthToeRoll/IK_RightFeet
    m_Weight: 0
  - m_Path: Player/Main/RightFeetCTRL/RightHillRoll/RIgthToeRoll/IK_RightFeet/IK_RightFeet_end
    m_Weight: 0
  - m_Path: Player_Armature
    m_Weight: 0
  - m_Path: Player_Armature.001
    m_Weight: 0
  - m_Path: Swing_2H_1
    m_Weight: 0
