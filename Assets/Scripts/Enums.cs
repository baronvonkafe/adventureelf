﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageType
{
    Slash,
    Pierce,
    Blunt,
    Magic,
    TrueDamage
}

public enum KnockbackType
{
    Flinch,
    None,
    Knockdown
}

public enum WeaponStyle
{
    Melee,
    Ranged
}

public enum WeaponRange
{
    FaceClose,
    Short,
    Normal,
    Big,
    Long
}

public enum WeaponType
{
    None,
    StraightSword,
    CurvedSword,
    Dagger,
    GreatSword,
    Hammer,
    Spead,
    Fist,
    Whip,
    Staff,
    Talisman,
    Shield
}

public enum ItemType
{
    Usable,
    Key,
    Equipment,
    Weapon,
    Spell,
    UpgradeMaterial
}

public enum UsableItemType
{
    None,
    Potion,
    Buff,
    ThrowingWeapon,
    Special
}

public enum HealingItemType
{
    None,
    Health,
    Spells,
    WeaponDurability
}

public enum EquipmentType
{
    Weapon,
    Hat,
    Chest,
    Gloves,
    Neclace,
    Pants,
    Shoes,
    Ring1,
    Ring2
}

public enum MeshBlendShape
{
    Chest,
    Gloves,
    Pants,
    Shoes
}

public enum SpellType
{
    None,
    Projectile,
    Area,
    Buff,
    Aura
}

public enum StatusAilment
{
    Poison,
    Bleed,
    SpellLocked,
    Slowed
}

public enum BuffType
{
    None,
    CastingSpeedUp,
    SpellProjectileSpeedUp,
    ThrowingWeaponDamageUp,
    ThrowingWeaponProjectileSpeedUp,
    NoSound,
    SilentCasting,
    InvisibleWeapon
}

public enum DebuffType
{
    None,
    MovementSpeedDown
}

public enum ElementType
{
    None,
    Wounding,           // Causes bleed
    Holy,
    Unholy,
    Poisonous,
    LightWeight,        // Wep: Low att, fast attack speed. Equip: Lower def, lower weight
    HeavyWeight,        // Same but reversed
    Interrupting,       // Iterrupts spells and skills
    Lucky,              // Crit up
    Magic,              // Causes magic dmg
    Countering,         // Counter dmg up
    Dispelling,         // Removes one buff
    Frost,
    Flaming,
    Cursed,             // Player also takes dmg
    Chaotic            // Has chance to cause random effects
}

public enum WeightType : int
{
    None,
    UltraLight,
    Light,
    Medium,
    Heavy,
    UltraHeavy
}