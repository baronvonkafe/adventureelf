﻿using UnityEngine;

public class Interactable : MonoBehaviour {

    public bool canInteractWith = true;
    public Collider interactTrigger;
    public string interactionText = "";

    public virtual void Interact()
    {
        
    }
}
