﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : Interactable {

    Animator animator;
    public InteractionTarget interactionTarget;

	void Start () {
        animator = GetComponent<Animator>();
        interactTrigger = GetComponent<Collider>();
	}

    public override void Interact()
    {
        if (!canInteractWith || interactTrigger == null || interactionTarget == null)
            return;

        if (!animator.IsInTransition(0))
        {    
            animator.SetTrigger("Activate");
            interactionTarget.Interact();
        }
    }
}
