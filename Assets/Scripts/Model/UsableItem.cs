﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Usable item", menuName = "Item/UsableItem")]
public class UsableItem : Item
{
    public UsableItemType usableType;
    public bool isEquipped = false;
    public int stack = 1;
    public int maxStack = 20;

    public override void Use()
    {
        stack--;
        base.Use();
    }
}
