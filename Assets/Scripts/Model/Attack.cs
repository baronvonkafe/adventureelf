﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Attack", menuName = "Attack")]
public class Attack : ScriptableObject
{
    new public string name;
    public string animationName;
    public DamageType damageType;
    public float damage;
    public float poiseDamage;
    public KnockbackType knockbackType;
}
