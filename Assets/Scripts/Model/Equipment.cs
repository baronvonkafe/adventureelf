using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Item/Equipment")]
public class Equipment : Item
{
    public EquipmentType equipmentType;
    public WeightType weightType;
    public BuffType buffType;
    public ElementType elementType;

    public GameObject prefab;
    public MeshBlendShape[] coveredMeshRegions;

    public float maxDurability;
    public float durability;
    
    // Used to generate base values
    public float physicalSlashDefModifier = 1.0f;
    public float physicalPierceDefModifier = 1.0f;
    public float physicalBluntDefModifier = 1.0f;
    public float magicDefModifier = 1.0f;

    public SkinnedMeshRenderer mesh;
    //public EquipmentManager.MeshBlendShape[] coveredMeshRegions;

    // Generated from balancing scripts.
    [ReadOnly] public float slashDef = 10;
    [ReadOnly] public float pierceDef = 10;
    [ReadOnly] public float bluntDef = 10;
    [ReadOnly] public float magicDef = 10;
    [ReadOnly] public float averageDefValues = 10;

    public float poisonRes;

    public bool isEquipped;

    public override void Use()
    {
        if(isEquipped)
        {
            isEquipped = false;
        }
        else
        {
            isEquipped = true;
        }

    }
}
