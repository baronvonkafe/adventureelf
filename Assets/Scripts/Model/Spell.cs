using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Spell", menuName = "Item/Spell")]
public class Spell : ScriptableObject
{
    public new string name;
    public SpellType spellType;
    public GameObject projectile;
    public GameObject particleEffect;
    
    public float speed;
    public float damage;
    public float size;
    public int charges;
    public float castSpeed;
    public bool canBeCharged;
}
