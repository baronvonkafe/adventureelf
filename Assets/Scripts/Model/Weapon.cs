﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Item/Weapon")]
public class Weapon : Equipment{

    public WeaponType weaponType;
    public WeaponRange weaponRange;
    public WeaponStyle weaponStyle;
    public List<Attack> lightAttackChain;
    public List<Attack> heavyAttackChain;
    public List<Attack> specialAttackChain;
    public Attack sprintAttack;
    public Attack airAttack;
    public Attack dodgeAttack;

    public Stat physicalBlockRate;
    public Stat magicBlockRate;

    public Vector3 spawnPosition;
    public Vector3 spawnRotation;
    public Vector3 spawnScale = Vector3.one;

}
