﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character stats", menuName = "Character stats")]
public class CharacterStats : ScriptableObject {

    public new string name;
    public int LVL;

    public float HP;
    public float EXP;
    public float maxEXP;

    public Stat maxHP;

    public int unAllocatedStats;
    public Stat STR;
    public Stat DEX;
    public Stat CON;
    public Stat INT;
    public Stat WIS;
    public Stat CHA;

    public Stat attack;
    public Stat slashDef;
    public Stat pierceDef;
    public Stat bluntDef;
    public Stat magicDef;
    public Stat movementSpeed;
    public Stat castingSpeed;

    public Stat spellAttunementSlots;
}
