﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bag : MonoBehaviour
{
    public GameObject bagSlotContainer;
    public GameObject bagSlotPrefab;
    public new string name;
    public int columns;
    public int rows;


    [ReadOnly] public int totalSlots;

    [ReadOnly] public bool isFull;

    public List<BagSlot> slots;
    public List<Item> items;

    private void Start()
    {
        totalSlots = rows * columns;
        bagSlotContainer.GetComponent<GridLayoutGroup>().constraintCount = columns;

        for (int i = 0; i < totalSlots; i++)
        {
            GameObject go = Instantiate(bagSlotPrefab, bagSlotContainer.transform);
            slots.Add(go.GetComponent<BagSlot>());
        }
    }

    public bool Add(Item item)
    {
        //Some checks?
        items.Add(item);

        return true;
    }

    public void Remove(Item item)
    {
        items.Remove(item);
    }

    public int getUsedSlots()
    {
        int isInUse = 0;
        for (int i = 0; i < slots.Count; i++)
        {
            if(slots[i].isInUse)
            {
                isInUse++;
            }
        }
        return isInUse;
    }

    public int getSlotsLeftInBag()
    {
        return totalSlots - getUsedSlots();
    }

}
