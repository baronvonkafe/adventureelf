﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : Interactable {

    Animator animator;
    public List<Item> items;
    public bool randomItem;
    public bool isLocked = false;
    bool itemGenerated = false;

    private void Start()
    {
        animator = GetComponent<Animator>();
        interactTrigger = GetComponent<Collider>();
    }

    public void OpenChest()
    {
        animator.SetBool("Open", true);

        if(!itemGenerated)
        {
            if (randomItem)
            {
                GenerateRandomItem();
            }
            else
            {
                if(items.Count > 0)
                {
                    SpawnItems();
                }
            }
        }
    }

    public void CloseChest()
    {
        animator.SetBool("Open", false);
    }

    public override void Interact()
    {
        base.Interact();
        if(!animator.IsInTransition(0))
        {
            if (animator.GetBool("Open"))
            {
                CloseChest();
            }
            else
            {
                if (!isLocked)
                {
                    OpenChest();
                }
            }
        }
    }

    private void SpawnItems()
    {

    }

    private void GenerateRandomItem()
    {

    }

}
