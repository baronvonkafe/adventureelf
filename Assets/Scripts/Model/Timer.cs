﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Timer
{
    [SerializeField] [ReadOnly] public float timer = 0;
    [SerializeField] public float maxTime = 1;
    [SerializeField] public bool isRunning;

    public Timer(float maxTime = 1)
    {
        isRunning = true;
        this.maxTime = maxTime;
    }

    public void TickFrame()
    {
        if(isRunning)
            timer += Time.deltaTime;
    }

    public bool IsTriggered()
    {
        bool returnVal = false;

        if (timer >= maxTime)
        {
            returnVal = true;
        }

        return returnVal;
    }

    public void Pause()
    {
        isRunning = false;
    }

    public void Resume()
    {
        isRunning = true;
    }

    public void Reset()
    {
        timer = 0;
    }

    public void Stop()
    {
        timer = 0;
        isRunning = false;
    }
}
