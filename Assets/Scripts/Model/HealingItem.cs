﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingItem : UsableItem
{
    public float amount;
    public HealingItemType healingItemType;


    public override void Use()
    {
        base.Use();
        switch (healingItemType)
        {
            case HealingItemType.None:
                break;
            case HealingItemType.Health:
                PlayerController.Instance.Heal(amount);
                break;
            case HealingItemType.Spells:
                break;
            case HealingItemType.WeaponDurability:
                break;
            default:
                break;
        }
    }
}
