﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonfire : Interactable {

    public GameObject stageEnemiesContainer;
    public Transform respawnPoint;

    private void Start()
    {
        //stageEnemiesContainer = Instantiate(stageEnemiesContainerPrefab);
    }

    public override void Interact()
    {
        base.Interact();
        Debug.Log("Bonfire sit");
        UIController.Instance.uiBonfire.Show();
        PlayerController.Instance.SitOnBonfire(respawnPoint);
        //StageController.Instance.LoadNewEnemies(stageEnemiesContainer);
        StageController.Instance.RespawnEnemies();
    }
}
