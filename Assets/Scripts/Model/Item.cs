﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Item/Item")]
public class Item : ScriptableObject
{
    new public string name;
    public ItemType type;
    public Sprite icon = null;
    [HideInInspector]
    public int slotsUsedRow = 1;
    [HideInInspector]
    public int slotsUsedColumn = 1;


    public string description;

    public virtual void Use()
    {

    }

    public int GetSlotsUsed()
    {
        return slotsUsedColumn * slotsUsedRow;
    }

    public void RemoveFromInventory()
    {
        InventoryController.Instance.Remove(this);
    }

}
