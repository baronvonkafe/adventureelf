﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanel : MonoBehaviour
{
    protected bool isShown;

    protected void Start()
    {
        Hide();
    }

    public virtual void Show()
    {
        isShown = true;
        this.gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        isShown = false;
        this.gameObject.SetActive(false);
    }

    public bool IsShown()
    {
        return isShown;
    }
}
