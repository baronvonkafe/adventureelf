﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIStatusBar : MonoBehaviour
{
    public TextMeshProUGUI lvlText;

    public RectTransform hpBar;
    public RectTransform hpBarBackground;
    public RectTransform expBar;
    public RectTransform expBarBackground;

    public void UpdateBars()
    {
        UpdateLVLLabel();
        UpdateHpBar();
        UpdateExpBar();
    }

    private void UpdateHpBar()
    {
        hpBar.sizeDelta = new Vector2(PlayerController.Instance.stats.HP, hpBar.sizeDelta.y);
        hpBarBackground.sizeDelta = new Vector2(PlayerController.Instance.stats.maxHP.GetValue(), hpBarBackground.sizeDelta.y);
    }

    private void UpdateExpBar()
    {
        expBar.sizeDelta = new Vector2(PlayerController.Instance.stats.EXP, expBar.sizeDelta.y);
        expBarBackground.sizeDelta = new Vector2(PlayerController.Instance.stats.maxEXP, expBarBackground.sizeDelta.y);
    }

    public void UpdateLVLLabel()
    {
        lvlText.text = "Lvl " + PlayerController.Instance.stats.LVL.ToString();
    }
}
