﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    #region Singleton
    public static InventoryController Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public delegate void OnItemChanged();
    public OnItemChanged onItemChangedCallback;

    public int maxBags = 2;

    public GameObject bagPrefab;

    public List<Bag> bags;

    public void Add(Item item)
    {
        if (onItemChangedCallback != null)
        {
            onItemChangedCallback.Invoke();
        }
    }

    public bool Remove(Item item)
    {
        for (int i = 0; i < bags.Count; i++)
        {
            for (int j = 0; j < bags[i].items.Count; j++)
            {
                if(bags[i].items[j] == item)
                {
                    //Remove item
                }
            }
        }
        return true;
    }

    public bool doesPlayerHaveItem(Item item)
    {
        for (int i = 0; i < bags.Count; i++)
        {
            for (int j = 0; j < bags[i].items.Count; j++)
            {

            }
        }
        return false;
    }

    public bool hasEnoughSpace(Item item)
    {
        bool enoughSpace = false;

        for (int i = 0; i < bags.Count; i++)
        {
            if (bags[i].getSlotsLeftInBag() >= item.GetSlotsUsed())
            {
                //if(bags[i].isRoomForCorrectShape())
                bags[i].Add(item);
                enoughSpace = true;
            }
        }

        return enoughSpace;
    }

}
