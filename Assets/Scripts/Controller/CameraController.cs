﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController Instance;

    [Header("Position to follow")]
    public Transform target;
    public GameObject player;

    private bool lockCursor = true;
    public float defaultmouseSensitivity = 10f;
    private float mouseSensitivity = 10f;
    public float zoomSensitivity = 5f;
    private float pitch;
    private float yaw;
    public Vector2 pitchMinMax = new Vector2(-40, 90);
    public Vector2 zoomMinMax = new Vector2(2, 5);

    public float rotationSmoothTime = 0.1f;
    Vector3 rotationSmoothVelocity;
    Vector3 currentRotation;

    public float zoomSmoothTime = 0.1f;
    public float zoomVelocity;
    public float currentZoom = 5f;
    float targetZoom = 10f;

    [Header("Collision check distance")]
    public float bumberDistanceCheck = 0.25f;
    public Vector3 bumperRayBottomOffset;
    public Vector3 bumperRayBackOffset;
    private Vector3 wantedPosition;
    //private float damping = 5f;

    [Header("Layers to collide")]
    public LayerMask camOcclusion;

    private Vector3 targetPlayerRotation;

    private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void LateUpdate()
    {
        UpdateInputs();

        RaycastHit hit = new RaycastHit();
        wantedPosition = target.position - transform.forward * currentZoom;
        bumperRayBottomOffset = new Vector3(wantedPosition.x, wantedPosition.y - bumberDistanceCheck, wantedPosition.z);
        bumperRayBackOffset = wantedPosition + (wantedPosition - target.position).normalized * bumberDistanceCheck;

        //if (Physics.Linecast(target.position, bumperRayBottomOffset, out hit, camOcclusion, QueryTriggerInteraction.Ignore))
        //{
        //    currentZoom = Vector3.Distance(target.position, hit.point);
        //}
        if (Physics.Linecast(target.position, bumperRayBackOffset, out hit, camOcclusion, QueryTriggerInteraction.Ignore))
        {
            wantedPosition = hit.point;
        }

        transform.position = wantedPosition;

        if (GameController.Instance.debug)
        {
            Debug.DrawLine(transform.position, target.position, Color.cyan);
            Debug.DrawLine(wantedPosition, target.position, Color.cyan);
            //Debug.DrawLine(transform.position, bumperRayBottomOffset, Color.red);
            Debug.DrawLine(transform.position, bumperRayBackOffset, Color.red);
        }

        RotatePlayer();
    }

    public void LockCursor(bool enable)
    {
        if(enable)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void UpdateInputs()
    {
        lockCursor = !UIController.Instance.IsMenuOpen();

        if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
        {
            lockCursor = false;
        }
        else if (Input.GetKeyUp(KeyCode.LeftAlt) || Input.GetKeyUp(KeyCode.RightAlt))
        {
            lockCursor = true;
            UIController.Instance.HideAllPanels();
        }

        LockCursor(lockCursor);
        //ChangeMouseSensitivity();

        if (lockCursor)
        {
            yaw += Input.GetAxis("Mouse X") * mouseSensitivity;
            pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;
            pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);

            currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);
            transform.eulerAngles = currentRotation;

            targetZoom -= Input.GetAxis("Mouse ScrollWheel") * zoomSensitivity;
            targetZoom = Mathf.Clamp(targetZoom, zoomMinMax.x, zoomMinMax.y);
            currentZoom = Mathf.SmoothDamp(currentZoom, targetZoom, ref zoomVelocity, zoomSmoothTime);
        }
    }

    private void RotatePlayer()
    {
        targetPlayerRotation = player.transform.eulerAngles;
        targetPlayerRotation.y = transform.eulerAngles.y;

        if (!PlayerController.Instance.isStunned)
        {
            player.transform.eulerAngles = targetPlayerRotation;
        }
    }

    private void ChangeMouseSensitivity()
    {
        if(PlayerController.Instance.isAttacking || PlayerController.Instance.GetIsRunning())
        {
            mouseSensitivity = defaultmouseSensitivity * 0.25f;
        }
        else
        {
            mouseSensitivity = defaultmouseSensitivity;
        }
    }
}

