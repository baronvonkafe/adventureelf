﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour{

    public static GameController Instance;

    public bool debug = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            debug = !debug;
            UIController.Instance.ToggleDebugPanel();
        }
        else if(Input.GetKeyDown(KeyCode.F2))
        {
            Time.timeScale = 0.5f;
        }
        else if (Input.GetKeyDown(KeyCode.F10))
        {
            UIController.Instance.ToggleUI();
        }
    }

}
