﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBonfire : UIPanel
{

    public void OpenLevelupUI()
    {

    }

    public void OpenSpellSelectUI()
    {

    }

    public void LeaveBonfire()
    {
        this.Hide();
        PlayerController.Instance.LeaveBonfire();
    }

}
