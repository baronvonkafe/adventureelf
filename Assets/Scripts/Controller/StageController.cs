﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{
    public static StageController Instance;

    public GameObject enemyContainer;
    public List<GameObject> enemies;
    public List<Transform> originalPositions; 

    private void Awake()
    {
        if (Instance == null)
        { 
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        for(int i = 0; i < enemyContainer.transform.childCount; i++)
        {
            enemies.Add(enemyContainer.transform.GetChild(i).gameObject);
            originalPositions.Add(enemyContainer.transform.GetChild(i));
        }
    }

    public void DeloadEnemies()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            Destroy(enemies[i]);
        }
        enemies.Clear();
        originalPositions.Clear();
    }

    public void LoadNewEnemies(GameObject newEnemiesPrefab)
    {
        int newEnemiesChildCount = newEnemiesPrefab.transform.childCount;

        for (int i = 0; i < newEnemiesChildCount; i++)
        {
            enemies.Add(Instantiate(newEnemiesPrefab.transform.GetChild(i).gameObject, enemyContainer.transform));
            originalPositions.Add(newEnemiesPrefab.transform.GetChild(i).GetComponent<EnemyComponent>().respawnPoint);
        }
    }

    public void ResetEnemies(GameObject newEnemiesPrefab)
    {
        DeloadEnemies();
        LoadNewEnemies(newEnemiesPrefab);
    }

    public void RespawnEnemies()
    {
        for (int i = 0; i < enemyContainer.transform.childCount; i++)
        {
            enemyContainer.transform.GetChild(i).GetComponent<EnemyComponent>().Respawn();
        }
    }

    public void RespawnToBonfire()
    {
        
    }
}
