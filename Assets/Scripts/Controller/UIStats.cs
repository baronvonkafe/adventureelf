﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIStats : MonoBehaviour
{
    public GameObject statPanelPrefab;
    [ReadOnly] public List<GameObject> statList;

    public void FillStatList()
    {
        GameObject go;

        go = Instantiate(statPanelPrefab, this.transform);
        go.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Lvl";
        go.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = PlayerController.Instance.stats.LVL.ToString();
        statList.Add(go);

        go = Instantiate(statPanelPrefab, this.transform);
        go.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "HP";
        go.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = PlayerController.Instance.stats.HP.ToString();
        statList.Add(go);
    }
}
