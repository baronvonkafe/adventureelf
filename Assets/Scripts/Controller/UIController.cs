﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIController : MonoBehaviour {

    public static UIController Instance;

    public GameObject   UI;
    public UIStatusBar  uiStatusBar;
    public UIInventory  uiInventory;
    public UIOptions    uiOptions;
    public UIBonfire    uiBonfire;

    public GameObject   gameMenuPanel;
    public GameObject   debugPanel;
    public GameObject   debugTextPanel;
    public GameObject   interactionPromtPanel;

    public GameObject   debugTextPrefab;

    private float       debugTimer = 0f;
    private float       debugWriteTime = 0.1f;
    private float       debugTimerLastCheck = 0;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            gameMenuPanel.SetActive(false);
            interactionPromtPanel.SetActive(true);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        debugPanel.SetActive(GameController.Instance.debug);

        if (interactionPromtPanel.activeSelf)
        {
            interactionPromtPanel.GetComponent<Animator>().SetBool("Active", false);
        }

        if (!UI.activeSelf)
        {
            ToggleUI();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            ToggleInventoryPanel();
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(uiBonfire.IsShown())
            {
                uiBonfire.LeaveBonfire();
            }
            else if(uiInventory.IsShown())
            {
                uiInventory.Hide();
            }
            else
            {
                ToggleGameMenuPanel();
            }
        }
    }

    private void LateUpdate()
    {
        // Switch to Delegate. More efficient.
        uiStatusBar.UpdateBars();
    }

    public void ToggleInventoryPanel()
    {
        if(uiInventory.IsShown())
        {
            uiInventory.Hide();
        }
        else
        {
            uiInventory.Show();
        }
    }

    public void ToggleGameMenuPanel()
    {
        gameMenuPanel.SetActive(!gameMenuPanel.activeSelf);
    }

    public void ToggleDebugPanel()
    {
        debugPanel.SetActive(!debugPanel.activeSelf);
    }

    public void ActivateInteractionPromptPanel(string text)
    {
        interactionPromtPanel.GetComponentInChildren<TextMeshProUGUI>().text = text;
        interactionPromtPanel.GetComponent<Animator>().SetBool("Active", true);
    }

    public void ActivateInteractionPromptPanel()
    {
        interactionPromtPanel.GetComponentInChildren<TextMeshProUGUI>().text = "Interact";
        interactionPromtPanel.GetComponent<Animator>().SetBool("Active", true);
    }

    public void DeActivateInteractionPanel()
    {
        interactionPromtPanel.GetComponent<Animator>().SetBool("Active", false);
    }

    public bool IsMenuOpen()
    {
        if (uiInventory.IsShown() || gameMenuPanel.activeSelf || uiBonfire.IsShown() || uiOptions.IsShown())
            return true;
        else
            return false;
    }

    public void HideAllPanels()
    {
        uiInventory.Hide();
        gameMenuPanel.SetActive(false);
    }

    public void Debug(string log)
    {
        if(debugTimer > debugWriteTime)
        {
            GameObject text = Instantiate(debugTextPrefab, debugTextPanel.transform);
            text.GetComponent<Text>().text = log;

            if (debugTextPanel.transform.childCount > 10)
            {
                Destroy(debugTextPanel.transform.GetChild(0).gameObject);
            }

            debugTimer = 0;
        }

        debugTimer += Time.time - debugTimerLastCheck;
        debugTimerLastCheck = Time.time;
    }

    public void ToggleUI()
    {
        UI.SetActive(!UI.activeSelf);
    }
}
