﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentController : MonoBehaviour
{

    public static EquipmentController Instance;

    public SkinnedMeshRenderer targetMesh;

    SkinnedMeshRenderer[] currentMeshes;
    Equipment[] currentEquipment;

    void Awake()
    {
        if (Instance == null)
        { 
            Instance = this;
        }
    }

    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem);
    public OnEquipmentChanged onEquipmentChanged;

    void Start()
    {
        int numSlots = System.Enum.GetNames(typeof(EquipmentType)).Length;
        currentEquipment = new Equipment[numSlots];
        currentMeshes = new SkinnedMeshRenderer[numSlots];
    }

    public void Equip(Equipment newItem)
    {
        int slotIndex = (int)newItem.equipmentType;

        Equipment oldItem = Unequip(slotIndex);

        if(onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newItem, oldItem);
        }

        currentEquipment[slotIndex] = newItem;
        AttackToMesh(newItem, slotIndex);
    }

    public Equipment Unequip(int slotIndex)
    {
        Equipment oldItem = null;

        if(currentEquipment[slotIndex] != null)
        {
            oldItem = currentEquipment[slotIndex];
            InventoryController.Instance.Add(oldItem);
            SetBlendShapeWeight(oldItem, 0);

            if(currentMeshes[slotIndex] != null)
            {
                Destroy(currentMeshes[slotIndex].gameObject);
            }

            currentEquipment[slotIndex] = null;

            if (onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldItem);
            }
        }

        return oldItem;
    }

    void AttackToMesh(Equipment item, int slotIndex)
    {
        SkinnedMeshRenderer newMesh = Instantiate(item.prefab.GetComponent<SkinnedMeshRenderer>()) as SkinnedMeshRenderer;
        newMesh.transform.parent = targetMesh.transform.parent;

        newMesh.rootBone = targetMesh.rootBone;
        newMesh.bones = targetMesh.bones;

        currentMeshes[slotIndex] = newMesh;

        SetBlendShapeWeight(item, 100);
    }

    void SetBlendShapeWeight(Equipment item, int weight)
    {
        foreach (MeshBlendShape blendShape in item.coveredMeshRegions)
        {
            int shapeIndex = (int)blendShape;
            targetMesh.SetBlendShapeWeight(shapeIndex, weight);
        }
    }

}
