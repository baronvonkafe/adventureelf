﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : UIPanel
{
    public override void Show()
    {
        base.Show();
        Time.timeScale = 0;
        PlayerController.Instance.isStunned = true;
    }

    public override void Hide()
    {
        base.Hide();
        Time.timeScale = 1;
        PlayerController.Instance.isStunned = false;
    }
}
