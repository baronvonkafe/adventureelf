﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMotor))]
public class PlayerController : CharacterComponent {

    public static PlayerController Instance;

    [HideInInspector] public CharacterMotor motor;
    private bool canInteract;
    public Interactable interactable;
    public Vector3 input;
    public Timer runningInputTimer;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Initialize();
    }

    public override void Initialize()
    {
        motor = GetComponent<CharacterMotor>();
        runningInputTimer = new Timer(0.1f);

        LoadStats();
        base.Initialize();
    }

    void LoadStats()
    {
        //if (SaveManager.Instance.SaveExists())
        //{
        //    CharacterStats = SaveManager.Instance.LoadStats();
        //    saveFileFound = true;
        //}
        //else
        //{
        //    stats = Instantiate(defaultStats);
        //}
        stats = Instantiate(defaultStats);
    }

    private void Update()
    {
        UpdateInputs();
        UpdateAnimationVariables();
    }

    void UpdateInputs()
    {
        if (isStunned)
            return;

        input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        motor.actualInputMoveDirection = input;
        motor.inputJump = Input.GetKey(KeyCode.Space);
        motor.canControl = !isAttacking;

        if (Input.GetMouseButtonDown(0) && isOnCombatMode)
        {
            combat.ActivateAttack(true, stats, animator);
        }
        else if (Input.GetMouseButtonDown(1) && isOnCombatMode)
        {
            combat.ActivateAttack(false, stats, animator);
        }

        if(!isAttacking)
        {
            if (Input.GetButtonUp("Interact"))
            {
                Interact();
            }
            else if (Input.GetKey(KeyCode.LeftControl))
            {
                if (input.z > 0.5)
                {
                    if (runningInputTimer.IsTriggered())
                    {
                        StartRunning();
                    }
                    else
                    {
                        runningInputTimer.TickFrame();
                    }
                }
                else
                {
                    StopRunning();
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                Dodge();
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                ToggleSneak();
            }
            else if(Input.GetKeyDown(KeyCode.X))
            {
                ToggleCombatMode();
            }

            DoACoolMemeSenpai();
        }
    }

    void ActivateSkill(int skillNumber)
    {

    }

    void ToggleSneak()
    {
        motor.movement.isSneaking = !motor.movement.isSneaking;
        animator.SetBool("Sit", motor.movement.isSneaking);
    }

    void Dodge()
    {
        if (!isDodging && !isAttacking)
        {
            isDodging = true;
            StopRunning();
            Vector3 dodgeDirection = input;

            if (dodgeDirection == Vector3.zero)
            {
                dodgeDirection = new Vector3(0, 0, -1);
            }

            animator.SetTrigger("Dodge");
            
            motor.movement.isSneaking = false;
            motor.movement.isRunning = false;
            motor.movement.isDodging = true;
        }
    }

    void StartRunning()
    {
        runningInputTimer.Reset();
        motor.movement.isRunning = true;
    }

    void StopRunning()
    {
        runningInputTimer.Reset();
        motor.movement.isRunning = false;
    }

    void PossiblyStopRunning()
    {
        if (input == Vector3.zero)
        {
            if (motor.movement.isRunning)
            {
                StopRunning();
            }
        }

        if (input.z <= 0.6f && motor.movement.isRunning)
        {
            StopRunning();
        }
    }

    void Interact()
    {
        if (canInteract && interactable != null)
        {
            interactable.Interact();
            UIController.Instance.DeActivateInteractionPanel();
        }
    }

    public void UpdateAnimationVariables()
    {
        animator.SetFloat("SpeedHorizontal", this.transform.InverseTransformDirection(motor.movement.velocity).x);
        animator.SetFloat("SpeedVertical", this.transform.InverseTransformDirection(motor.movement.velocity).z);
        animator.SetFloat("InputHorizontal", this.input.x);
        animator.SetFloat("InputVertical", this.input.z);
        animator.SetFloat("SpeedFalling", this.transform.InverseTransformDirection(motor.movement.velocity).y);
        animator.SetBool("Jump", !motor.IsGrounded());

        isAttacking = combat.GetIsAttacking(animator);
        isDodging = combat.GetIsDodging(animator);
    }

    public void SetCanControl(bool canControl)
    {
        motor.canControl = canControl;
    }
    
    public bool GetCanControl()
    {
        return motor.canControl;
    }

    public bool GetIsRunning()
    {
        return motor.movement.isRunning;
    }

    public void SitOnBonfire(Transform respawnPoint)
    {
        // Turn towards the bonfire
        Debug.Log("SitOnBonFire");
        FullHeal();
        respawnPointPosition = respawnPoint.position;
        // Restore spells
        // Restore potions and bombs
        animator.SetBool("Sit", true);
        motor.canControl = false;
        isStunned = true;
    }

    public void LeaveBonfire()
    {
        animator.SetBool("Sit", false);
        motor.canControl = true;
        isStunned = false;
    }

    public override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        if(other.CompareTag("Interactable") && other.GetComponentInParent<Interactable>() != null)
        {
            Debug.Log(other.GetComponentInParent<Interactable>().interactionText);
            if(other.GetComponentInParent<Interactable>().interactionText == "")
            {
                UIController.Instance.ActivateInteractionPromptPanel();
            }
            else
            {
                UIController.Instance.ActivateInteractionPromptPanel(other.GetComponentInParent<Interactable>().interactionText);
            }
            FillInteractable(other.GetComponentInParent<Interactable>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(this.tag))
            return;

        if(other.CompareTag("Interactable"))
        {
            canInteract = false;
            interactable = null;
            UIController.Instance.DeActivateInteractionPanel();
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(CompareTag("Enemy"))
        {
            Debug.Log(hit.gameObject.tag);
        }
    }

    public void GetHitByAttack(Attack attack)
    {
        if (godMode)
            return;

        Debug.Log(attack.name);
        TakeDamage(attack.damage, attack.damageType);
        //check if knockback occures
        //play flinch animation
        //UpdateHealthBar();

        if (stats.HP <= 0)
        {
            Die();
        }
    }

    public void SetPlayerAnimationSpeed(float speed)
    {
        animator.speed = speed;
    }

    /// <summary>
    /// Activated on animation events just as blade is about to hit.
    /// </summary>
    public void HitBoxActivate()
    {
        combat.ActivateCurrentAttack();
        animator.SetBool("LightAttack", false);
        animator.SetBool("HeavyAttack", false);
        animator.SetBool("SpecialAttack", false);
    }

    /// <summary>
    /// Activated on animation events after attacking has been done.
    /// </summary>
    public void HitBoxDeactivate()
    {
        combat.DisableHitbox();
    }

    private void FillInteractable(Interactable inter)
    {
        canInteract = true;
        interactable = inter;
    }

    public override void Die()
    {
        base.Die();
        isStunned = true;
        motor.movement.StopMovement();
        CameraFade.Instance.BlinkScreen(1f, 1f);
        this.transform.position = respawnPoint.position;
        isStunned = false;
        StageController.Instance.RespawnToBonfire();
    }

    private void DoACoolMemeSenpai()
    {
        if (Input.GetKey(KeyCode.F11))
        {
            animator.SetBool("Dab", true);
        }
        else
        {
            animator.SetBool("Dab", false);
        }

        if(Input.GetKey(KeyCode.P))
        {
            animator.SetTrigger("TestAnimation");
        }
    }

    public void ToggleCombatMode()
    {
        float layerWeight = animator.GetLayerWeight(1);

        if(layerWeight > 0.8f)
        {
            layerWeight = 0f;
        }
        else
        {
            layerWeight = 1f;
        }

        animator.SetLayerWeight(1, layerWeight);
    }

}
