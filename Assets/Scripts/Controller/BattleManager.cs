﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    public static BattleManager Instance;

    public float difficultyLevel;
    public List<GameObject> enemiesInBattle;
    public float averageEnemyLevel;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(enemiesInBattle.Count > 0)
        {

        }
    }

    public void AddEnemyToBattle(GameObject enemy)
    {
        enemiesInBattle.Add(enemy);
        CalculateAverageEnemyLevel();
        CalculateDifficultyLevel();
    }

    private void CalculateAverageEnemyLevel()
    {
        float totalLevels = 0;

        foreach (GameObject enemy in enemiesInBattle)
        {
            totalLevels = enemy.GetComponent<EnemyComponent>().stats.LVL;
        }
        averageEnemyLevel = totalLevels / enemiesInBattle.Count;
    }

    private void CalculateDifficultyLevel()
    {
        // Consider enemy count, are they lower level than player
        difficultyLevel = 1;
    }
}
