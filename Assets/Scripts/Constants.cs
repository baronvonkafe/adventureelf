﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants {

    public static readonly float MaxLVL = 255f;
    public static readonly float defaultKnockbackForce = 25f;
    public static readonly int maxWeaponSlots;
}
