﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class StateMachine : MonoBehaviour
{
    private Dictionary<Type, BaseState> availableStates;
    public BaseState currentState;
    public event Action<BaseState> OnStateChanged;
    public float updateTimeSeconds;
    public bool useSlowMode = false;
    public TextMeshProUGUI debugPhaseText;

    public void AddState(Type type, BaseState state)
    {
        if(availableStates == null)
        {
            availableStates = new Dictionary<Type, BaseState>();
            availableStates.Add(type, state);
            currentState = availableStates.Values.First();
            debugPhaseText.text = getCurrentStateName();
        }
        else
        {
            availableStates.Add(type, state);
        }
    }

    public void SetStates(Dictionary<Type, BaseState> states)
    {
        availableStates = states;
    }

    public void StartStateMachine()
    {
        if(useSlowMode)
        {
            StartCoroutine(UpdateSlowly());
        }
    }

    void Update()
    {
        if (useSlowMode)
            return;

        if (availableStates == null)
            return;

        UpdateStateMachine();
    }

    private IEnumerator UpdateSlowly()
    {
        while (true)
        {
            UpdateStateMachine();

            yield return new WaitForSeconds(updateTimeSeconds);
        }
    }

    private void UpdateStateMachine()
    {
        if (currentState == null)
        {
            currentState = availableStates.Values.First();
        }

        var nextState = currentState?.Tick();

        if (nextState != null && nextState != currentState?.GetType())
        {
            SwitchToNewState(nextState);
        }
    }

    private void SwitchToNewState(Type nextState)
    {
        currentState = availableStates[nextState];
        debugPhaseText.text = getCurrentStateName();
        OnStateChanged?.Invoke(currentState);
    }

    public string getCurrentStateName()
    {
        return currentState.ToString() + currentState.PrintDebug();
    }

}
