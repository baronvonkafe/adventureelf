﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : BaseState
{
    EnemyComponent idler;

    public IdleState(EnemyComponent idler) : base(idler.gameObject)
    {
        this.idler = idler;
    }

    public override void Enter()
    {
        
    }

    public override Type Tick()
    {
        if (true)
        {
            if(ScanForAggro())
            {
                BattleManager.Instance.AddEnemyToBattle(this.gameObject);
                idler.AggroToPlayer(PlayerController.Instance.transform);
            }
        }

        return null;
    }

    public bool ScanForAggro()
    {
        if (idler.aggroDistance > idler.DistanceToPlayer())
        {
            if (!Physics.Linecast(this.transform.position, PlayerController.Instance.transform.position))
            {
                return true;
            }
        }
        return false;
    }

    public override string PrintDebug()
    {
        return "";
    }
}
