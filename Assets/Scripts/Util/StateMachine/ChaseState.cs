﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChaseState : BaseState
{
    EnemyComponent chaser;
    Transform target;

    public ChaseState(EnemyComponent chaser, Transform target) : base(chaser.gameObject)
    {
        this.chaser = chaser;
        this.target = target;
    }

    public override void Enter()
    {

    }

    public override Type Tick()
    {
        ChasePlayer();

        return typeof(ChaseState);
    }

    private void ChasePlayer()
    {
        chaser.agent.updatePosition = true;
        chaser.agent.SetDestination(target.position);

        if (chaser.agent.pathStatus == NavMeshPathStatus.PathComplete && chaser.agent.remainingDistance < CalculateFightingDistance())
        {
            //SetEnemyPhase(EnemyPhase.Fighting);
        }
    }

    public void ChangeTarget(Transform newTarget)
    {
        this.target = newTarget;
    }

    public override string PrintDebug()
    {
        return "";
    }

    private float CalculateFightingDistance()
    {
        if (chaser.weapons[0].weaponStyle == WeaponStyle.Melee)
        {
            return Util.GetFightingDistance(chaser.weapons[0].weaponRange);
        }
        return 9999999f;
    }
}
