﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(LineRenderer))]
public class NavAgentLineRenderer : MonoBehaviour
{

    public bool enableLineRendering;
    private NavMeshAgent agent;
    private LineRenderer lineRenderer;

    void Start()
    {
        GetComponent<NavAgentLineRenderer>().enabled = enableLineRendering;

        agent = GetComponent<NavMeshAgent>();
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Update()
    {
        if(agent.hasPath)
        {
            lineRenderer.enabled = true;
            lineRenderer.positionCount = agent.path.corners.Length;
            lineRenderer.SetPositions(agent.path.corners);
        }
        else
        {
            lineRenderer.enabled = false;
        }
    }
}
