﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util {

    public static string FormatDecimal(double value)
    {
        return value.ToString("F3");
    }

    public static void AddHitboxActivationToAnimation(Animator anim, string clipName, int start, int end)
    {
        float timeStart = (float)start / 24;
        float timeEnd = (float)end / 24;
        int clipIndex = GetAnimationIndex(anim, clipName);
        AnimationEvent animationEvent = new AnimationEvent();
        AnimationClip clip = anim.runtimeAnimatorController.animationClips[clipIndex];
        animationEvent.functionName = "HitBoxActivate";
        animationEvent.floatParameter = 0;
        animationEvent.time = timeStart;
        clip.AddEvent(animationEvent);

        animationEvent.functionName = "HitBoxDeactivate";
        animationEvent.time = timeEnd;
        clip.AddEvent(animationEvent);
    }

    public static void AddAnimationEvent(Animator anim, int clipIndex, float time, string functionName, float floatParameter)
    {
        AnimationEvent animationEvent = new AnimationEvent();
        animationEvent.functionName = functionName;
        animationEvent.floatParameter = floatParameter;
        animationEvent.time = time;
        AnimationClip clip = anim.runtimeAnimatorController.animationClips[clipIndex];
        clip.AddEvent(animationEvent);
    }

    public static void AddAnimationEvent(Animator anim, string clipName, float time, string functionName, float floatParameter)
    {
        AnimationEvent animationEvent = new AnimationEvent();
        animationEvent.functionName = functionName;
        animationEvent.floatParameter = floatParameter;
        animationEvent.time = time;
        AnimationClip clip = anim.runtimeAnimatorController.animationClips[GetAnimationIndex(anim, clipName)];
        clip.AddEvent(animationEvent);
    }

    public static int GetAnimationIndex(Animator anim, string clipName)
    {
        int index = 0;
        foreach (AnimationClip clip in anim.runtimeAnimatorController.animationClips)
        {
            if(clip.name == clipName)
            {
                return index;
            }
            index++;
        }
        Debug.LogError("Animation " + clipName + " not found in animator " + anim.name + "!");
        return 0;
    }

    public static bool GetIsAnimationPlaying(Animator anim, int layer, string animName)
    {
        bool isPlaying = false;

        if (anim.GetCurrentAnimatorStateInfo(layer).IsName(animName))
        {
            isPlaying = true;
        }

        return isPlaying;
    }

    public static float GetFightingDistance(WeaponRange range)
    {
        float attackRange = 0f;

        switch (range)
        {
            case WeaponRange.FaceClose:
                attackRange = 0.2f;
                break;
            case WeaponRange.Short:
                attackRange = 1f;
                break;
            case WeaponRange.Normal:
                attackRange = 3f;
                break;
            case WeaponRange.Big:
                attackRange = 5f;
                break;
            case WeaponRange.Long:
                attackRange = 15f;
                break;
            default:
                break;
        }

        return attackRange;
    }
}
