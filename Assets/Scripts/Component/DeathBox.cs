﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBox : MonoBehaviour
{

    private Collider boxCollider;

    void Start()
    {
        boxCollider = this.GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(this.tag))
            return;

        if(other.CompareTag("Player"))
        {
            other.GetComponent<PlayerController>().Die();
        }
    }

}
