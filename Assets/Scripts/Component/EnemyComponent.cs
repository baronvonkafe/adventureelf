﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyPhase
{
    Idle,
    Patrol,
    Chase,
    Fighting,
    Attacking,
    Defending,
    Knockback,
    Death,
    BeggingForMercy,
    Pacified
}

public enum EnemyPersonality
{
    Normal,
    Turret,
    Aggressive,
    Defensive,
    Coward
}

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof(StateMachine))]
public class EnemyComponent : CharacterComponent
{
    private const float enemyThinkTime = 0.2f;

    public bool isHostile;
    public bool isAggroed;
    public float rotationSpeed = 1;
    public float aggroDistance;
    [ReadOnly] public float maxFightingDistance;
    private float fightingDistance;
    public bool isRespwanable;
    private int totalTimesKilled;
    private Transform aggroTarget;
    public UnityEngine.AI.NavMeshAgent agent => GetComponent<UnityEngine.AI.NavMeshAgent>();
    public StateMachine stateMachine => GetComponent<StateMachine>();
    public EnemyPersonality personality;
    private EnemyPhase enemyPhase;
    public GameObject damageNumbersPrefab;

    private void Start()
    {
        Initialize();
    }

    public override void Initialize()
    {
        respawnPoint = this.transform;
        respawnPointPosition = this.transform.position;
        respawnPointRotation = this.transform.rotation;
        hpBarMaxLenght = hpBar.sizeDelta.x;
        base.Initialize();
        isKnockedback = false;
        stats = Instantiate(defaultStats);
        InitializeStateMachine();
    }

    private void InitializeStateMachine()
    {
        stateMachine.AddState(typeof(IdleState), new IdleState(this));
        stateMachine.AddState(typeof(ChaseState), new ChaseState(this, aggroTarget));

        stateMachine.StartStateMachine();
    }

    private IEnumerator UpdateEnemy()
    {
        while (enemyPhase != EnemyPhase.Death)
        {
            if(!isKnockedback)
            {
                switch (enemyPhase)
                {
                    case EnemyPhase.Idle:
                        ScanForAggro();
                        break;
                    case EnemyPhase.Patrol:
                        break;
                    case EnemyPhase.Fighting:
                        FightingPlayer();
                        break;
                    case EnemyPhase.Attacking:
                        break;
                    case EnemyPhase.Defending:
                        break;
                    case EnemyPhase.Knockback:
                        StartCoroutine(GetKnockedBack());
                        break;
                    case EnemyPhase.Death:
                        break;
                    default:
                        break;
                }
            }

            yield return new WaitForSeconds(enemyThinkTime);
        }
    }

    public void ScanForAggro()
    {
        if(aggroDistance > DistanceToPlayer())
        {
            if (!Physics.Linecast(this.transform.position, PlayerController.Instance.transform.position))
            {
                BattleManager.Instance.AddEnemyToBattle(this.gameObject);
                AggroToPlayer(PlayerController.Instance.transform);
            }
        }
    }

    public override void Die()
    {
        LoseAggro();
        PlayerController.Instance.GainExp(stats.EXP);

        // Play death animation
        // Play death sound

        totalTimesKilled++;
        GetComponent<EnemyComponent>().enabled = false;
    }

    public void AggroToPlayer(Transform playerTarget)
    {
        if (aggroTarget == playerTarget)
            return;

        if (isHostile)
        {
            isAggroed = true;
            aggroTarget = playerTarget;
            agent.SetDestination(aggroTarget.position);
        }
    }

    public void LoseAggro()
    {
        isAggroed = false;
        aggroTarget = null;
    }

    public void LookTowardsAggroTarget()
    {
        Vector3 targetDirection = Vector3.zero;

        while (isAggroed)
        {
            if (!isStunned)
            {
                targetDirection = aggroTarget.position - transform.position;
                targetDirection.y = 0f;
                this.transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(targetDirection), Time.time * agent.angularSpeed);
            }
        }
    }

    private void FightingPlayer()
    {
        //LookTowardsAggroTarget();
        //agent.updatePosition = false;
        //agent.SetDestination(aggroTarget.position);

        if (DistanceToPlayer() > maxFightingDistance)
        {
            //SetEnemyPhase(EnemyPhase.Chase);
        }

        // Dance around player
    }

    private IEnumerator GetKnockedBack()
    {
        isKnockedback = true;
        agent.angularSpeed = 0;
        agent.velocity = knockbackDirection * knockbackForce;
        yield return new WaitForSeconds(0.2f);
        isKnockedback = false;
    }

    public void Respawn()
    {
        this.gameObject.SetActive(true);
        FullHeal();
        UpdateHealthBar();
        isStunned = false;
        isAggroed = false;
        isKnockedback = false;
        this.transform.position = respawnPointPosition;
        this.transform.rotation = respawnPointRotation;
    }

    public override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
    }

    public override void TakeKnockback(Vector3 attackerPosition, KnockbackType knockbackType)
    {

        base.TakeKnockback(attackerPosition, knockbackType);

    }

    public void SetEnemyPhase(EnemyPhase phase)
    {
        enemyPhase = phase;
        
        animator.SetInteger("EnemyPhase", (int)enemyPhase);
    }

    public float DistanceToPlayer()
    {
        return Vector3.Distance(this.transform.position, PlayerController.Instance.transform.position);
    }

}
