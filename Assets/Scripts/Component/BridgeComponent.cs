﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeComponent : InteractionTarget
{

    private Animator animator;
    public bool raiseBridge;

    void Start()
    {
        animator = this.GetComponent<Animator>();
        RaiseBridge();
    }

    public override void Interact()
    {
        base.Interact();
        raiseBridge = !raiseBridge;
        RaiseBridge();
    }

    public void RaiseBridge()
    {
        animator.SetBool("RaiseBridge", raiseBridge);
    }
}
