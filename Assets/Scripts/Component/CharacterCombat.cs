﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterComponent))]
public class CharacterCombat : MonoBehaviour
{

    private enum AttackType
    {
        None,
        LightAttack,
        HeavyAttack,
        SpecialAttack,
        SprintAttack,
        AirAttack,
        DodgeAttack
    }

    [ReadOnly] public GameObject weaponPrefab;
    [ReadOnly] public Weapon weapon;
    [ReadOnly] public Collider hitbox;
    [ReadOnly] public int currectComboIndex = 0;
    [ReadOnly] private Attack currentAttack;
    [ReadOnly] private AttackType nextAttackType;

    public void SwapWeapon(Weapon currentWeapon, Transform rightHandGrab)
    {
        weapon = currentWeapon;

        if (weaponPrefab != null)
        {
            Destroy(weaponPrefab);
        }

        weaponPrefab = Instantiate(currentWeapon.prefab, rightHandGrab);
        weaponPrefab.transform.localPosition = weapon.spawnPosition;
        weaponPrefab.transform.localEulerAngles = weapon.spawnRotation;
        weaponPrefab.transform.localScale = weapon.spawnScale;
        
        hitbox = weaponPrefab.GetComponentInChildren<Collider>();
        hitbox.isTrigger = true;
        hitbox.enabled = false;
        currentAttack = weapon.lightAttackChain[currectComboIndex];
        DisableHitbox();
    }

    public void ToggleCombatMode(Transform spawnTransform)
    {
        weaponPrefab.transform.SetParent(spawnTransform);
    }

    private void EnableHitbox()
    {
        hitbox.enabled = true;
    }

    public void DisableHitbox()
    {
        hitbox.enabled = false;
    }

    public void ActivateAttack(bool lightAttack, CharacterStats stats, Animator animator)
    {
        if (lightAttack)
        {
            QueueLightAttack(animator);
        }
        else
        {
            QueueHeavyAttack(animator);
        }
    }

    public void ActivateSpecialAttack(CharacterStats stats, Animator animator)
    {

    }

    public void QueueLightAttack(Animator animator)
    {
        nextAttackType = AttackType.LightAttack;
        animator.SetBool("LightAttack", true);
    }

    public void QueueHeavyAttack(Animator animator)
    {
        nextAttackType = AttackType.HeavyAttack;
        animator.SetBool("HeavyAttack", true);
    }

    public void QueueSpecialAttack(Animator animator, int specialAttackIndex)
    {
        nextAttackType = AttackType.SpecialAttack;
        animator.SetInteger("Skill", specialAttackIndex);
    }

    public void ActivateCurrentAttack()
    {
        UpdateComboIndex();
        UpdateCurrentAttack();
        EnableHitbox();
    }

    private void UpdateComboIndex()
    {
        currectComboIndex++;

        switch (nextAttackType)
        {
            case AttackType.LightAttack:
                if (currectComboIndex >= weapon.lightAttackChain.Count)
                {
                    currectComboIndex = 0;
                }
                break;
            case AttackType.HeavyAttack:
                if (currectComboIndex >= weapon.heavyAttackChain.Count)
                {
                    currectComboIndex = 0;
                }
                break;
            case AttackType.SpecialAttack:
                if (currectComboIndex >= weapon.specialAttackChain.Count)
                {
                    currectComboIndex = 0;
                }
                break;
            default:
                break;
        }
    }

    public void ResetCurrentComboIndex()
    {
        currectComboIndex = 0;
    }

    private void UpdateCurrentAttack()
    {
        switch (nextAttackType)
        {
            case AttackType.None:
                Debug.LogError("Error: attack type not queued!");
                break;
            case AttackType.LightAttack:
                currentAttack = weapon.lightAttackChain[currectComboIndex];
                break;
            case AttackType.HeavyAttack:
                currentAttack = weapon.heavyAttackChain[currectComboIndex];
                break;
            case AttackType.SpecialAttack:
                currentAttack = weapon.specialAttackChain[currectComboIndex];
                break;
            case AttackType.SprintAttack:
                currentAttack = weapon.sprintAttack;
                break;
            case AttackType.AirAttack:
                currentAttack = weapon.airAttack;
                break;
            case AttackType.DodgeAttack:
                currentAttack = weapon.dodgeAttack;
                break;
            default:
                break;
        }

        if (currentAttack == null)
        {
            Debug.LogError("Error: current attack is null!");
        }
    }

    public bool GetIsAttacking(Animator animator)
    {
        bool isAttacking = false;

        if (animator.GetCurrentAnimatorStateInfo(2).IsName("Idle") ||
            animator.GetCurrentAnimatorStateInfo(2).IsName("Dodge"))
        {
            ResetCurrentComboIndex();
        }
        else
        {
            isAttacking = true;
        }

        return isAttacking;
    }

    public bool GetIsDodging(Animator animator)
    {
        bool isDodging = false;

        if(animator.GetCurrentAnimatorStateInfo(2).IsName("Dodge"))
        {
            isDodging = true;
        }

        return isDodging;
    }

    public Attack GetCurrentAttack()
    {
        return this.currentAttack;
    }
}
