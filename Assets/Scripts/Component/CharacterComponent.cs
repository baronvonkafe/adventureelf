﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterCombat))]
public class CharacterComponent : MonoBehaviour {

    [HideInInspector] public Animator animator;
    public CharacterStats defaultStats;
    [HideInInspector] public CharacterCombat combat;
    public Weapon defaultWeapon;
    [HideInInspector] public CharacterStats stats;
    public List<Equipment> equipments = new List<Equipment>();
    public List<Weapon> weapons = new List<Weapon>();
    public Transform leftHandGrab;
    public Transform rightHandGrab;
    public Transform sheathWeaponTransform;
    [ReadOnly] public int currentWeaponIndex = 0;
    public Transform respawnPoint;
    [ReadOnly] public Vector3 respawnPointPosition;
    [ReadOnly] public Quaternion respawnPointRotation;
    public GameObject hPBarCanvas;
    public RectTransform hpBar;
    public RectTransform hpBarBackground;
    public bool godMode;
    protected float hpBarMaxLenght;
    [HideInInspector] protected bool isKnockedback;
    [HideInInspector] protected Vector3 knockbackDirection;
    [HideInInspector] protected float knockbackForce;

    [HideInInspector] public bool isAttacking = false;
    [HideInInspector] public bool isDodging = false;
    [HideInInspector] public bool isStunned = false;
    [HideInInspector] public bool twoHanded = false;
    public bool isOnCombatMode = true;

    public virtual void Initialize()
    {
        animator = GetComponent<Animator>();
        combat = GetComponent<CharacterCombat>();

        if (stats == null)
        {
            Debug.LogError("STATS MISSING FROM " + name);
        }

        if (weapons[0] == null)
        {
            weapons[0] = defaultWeapon;

            if(weapons[0] == null)
            {
                Debug.LogError("DEFAULT WEAPON MISSING FROM " + name);
            }
        }

        if(rightHandGrab == null)
        {
            Debug.LogError("RIGHT HAND GRAB FOR WEAPON MISSING FROM " + name);
        }

        combat.SwapWeapon(weapons[0], rightHandGrab);
    }

    public void Heal(float amount)
    {
        stats.HP += amount;
        if(stats.HP > stats.maxHP.GetValue())
        {
            stats.HP = stats.maxHP.GetValue();
        }
    }

    public void FullHeal()
    {
        stats.HP = stats.maxHP.GetValue();
    }


    public void TakeDamage(float amount, DamageType damageType)
    {
        if (godMode)
            return;

        float dmgAfterDefence = amount;

        switch (damageType)
        {
            case DamageType.Slash:
                dmgAfterDefence = CalculateDamageReduction(amount, stats.slashDef.GetValue());
                break;
            case DamageType.Pierce:
                break;
            case DamageType.Blunt:
                break;
            case DamageType.Magic:
                break;
            case DamageType.TrueDamage:
                break;
            default:
                break;
        }

        stats.HP -= amount;
    }

    public virtual void TakeKnockback(Vector3 attackerPosition, KnockbackType knockbackType)
    {
        if (godMode)
            return;

        Vector3 dir = this.transform.position - attackerPosition;

        if (knockbackType == KnockbackType.Knockdown)
        {
            //motor.AddImpact(dir);
            isKnockedback = true;
        }
        else if(knockbackType == KnockbackType.Flinch)
        {
            //animator.setTrigger("Flinch");
        }
    }

    public virtual void Die()
    {
        //Play death animation
    }

    public void UpdateDefences()
    {
        stats.slashDef.SetBaseValue(0);
        stats.pierceDef.SetBaseValue(0);
        stats.bluntDef.SetBaseValue(0);
        stats.magicDef.SetBaseValue(0);

        for (int i = 0; i < equipments.Count; i++)
        {
            stats.slashDef.AddModifier(equipments[i].slashDef);
            stats.pierceDef.AddModifier(equipments[i].pierceDef);
            stats.bluntDef.AddModifier(equipments[i].bluntDef);
            stats.magicDef.AddModifier(equipments[i].magicDef);
        }
    }

    public float CalculateDamageReduction(float dmg, float def)
    {
        // create damage reduction formula here
        return dmg;
    }

    public void GainExp(float exp)
    {
        stats.EXP += exp;

        if (stats.EXP >= stats.maxEXP)
        {
            stats.EXP -= stats.maxEXP;
            stats.LVL++;
            stats.unAllocatedStats++;
            float temp = Mathf.Pow(1.15f, stats.LVL);
            stats.maxEXP = Mathf.Floor(10 * temp);
        }
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);

        if (other.transform.IsChildOf(this.transform))
            return;

        if (other.CompareTag("Weapon"))
        {
            CharacterCombat combat = null;
            combat = GetCharacterCombatFromWeaponCollider(other.transform);

            if(combat != null)
            {
                if (other.transform.parent != null)
                {
                    GetHitByAttack(other.transform.position, combat.GetCurrentAttack());
                }
                else
                {
                    GetHitByAttack(other.transform.position, combat.GetCurrentAttack());
                }
            }
            else
            {
                Debug.LogError(other.name + " cannot find CharacterCombat in parents!");
            }
        }
        else if(other.CompareTag("Attack"))     // Traps, explosions, falling rocks, etc.
        {

        }
    }

    public virtual void GetHitByAttack(Vector3 attackerPosition, Attack attack)
    {
        TakeDamage(attack.damage, attack.damageType);

        if (attack.knockbackType != KnockbackType.None)
            TakeKnockback(attackerPosition, attack.knockbackType);
        
        //play flinch animation
        UpdateHealthBar();
        PopupDamageNumber();

        if (stats.HP <= 0)
        {
            Die();
        }
    }

    public virtual void UpdateHealthBar()
    {
        float healthPercent = stats.HP / stats.maxHP.GetValue();
        hpBar.sizeDelta = new Vector2(healthPercent * hpBarMaxLenght, hpBar.sizeDelta.y);

        if (stats.HP >= stats.maxHP.GetValue())
        {
            //hPBarCanvas.SetActive(false);
        }
        else
        {
            hPBarCanvas.SetActive(true);
        }
    }

    void PopupDamageNumber()
    {

    }

    public void ToggleTwoHanded()
    {
        twoHanded = !twoHanded;

        animator.SetBool("TwoHanded", twoHanded);
    }

    public void EquipWeapon(Weapon newWeapon, int slotNumber)
    {
        if (slotNumber >= Constants.maxWeaponSlots)
            slotNumber = Constants.maxWeaponSlots;
        else if (slotNumber < 0)
            slotNumber = 0;

        if (slotNumber == currentWeaponIndex)
        {
            // Remove weapon model
            // Remove buffs and bonuses from current weapon
            // Add new buffs and bonuses
            // Add new weapon model
        }

        weapons[slotNumber] = newWeapon;

    }

    public void SwitchWeapon(int slotNumber)
    {
        if (slotNumber >= Constants.maxWeaponSlots)
            slotNumber = Constants.maxWeaponSlots;
        else if (slotNumber < 0)
            slotNumber = 0;

        currentWeaponIndex = slotNumber;

        // Remove buffs and bonuses
        // Add new buffs and bonuses
    }

    private CharacterCombat GetCharacterCombatFromWeaponCollider(Transform collider)
    {
        Component[] colliderParents;
        CharacterCombat retValue = null;

        colliderParents = collider.GetComponentsInParent(typeof(Transform));

        if (colliderParents != null)
        {
            foreach (Transform item in colliderParents)
            {
                if (item.CompareTag("Player") || item.CompareTag("Enemy"))
                {
                    retValue = item.GetComponent<CharacterCombat>();
                }
            }
        }

        return retValue;
    }
}
